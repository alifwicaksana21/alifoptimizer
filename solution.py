class Solution:
    def __init__(self, params: list):
        self.n = len(params)
        self.params = params
        self.velocity = None
        self.fitness = None
    
    def setParams(self, params: list):
        self.params = params
    
    def setFitness(self, fitness: float):
        self.fitness = fitness
    
    def __str__(self):
        return f"Solution({self.params} => {self.fitness})"