import random
import numpy as np
import math

class Particle:
    def __init__(self, objective_function, lb, ub):
        self.position = np.array([random.uniform(lb[j], ub[j]) for j in range(len(lb))])
        self.velocity = np.zeros(len(lb))
        self.best_position = self.position.copy()
        self.best_fitness = np.inf
        self.fitness = objective_function(self.position)
        self.objective_function = objective_function
        self.lb = lb
        self.ub = ub
    
    def update_velocity(self, global_best_position, w, c1, c2):
        self.velocity = w*self.velocity + c1*random.uniform(0, 1)*(self.best_position - self.position) + c2*random.uniform(0, 1)*(global_best_position - self.position)
    
    def update_position(self):
        self.position = np.clip(self.position + self.velocity, self.lb, self.ub)
        self.fitness = self.objective_function(self.position)
    
    def update_personal_best(self):
        if self.fitness < self.best_fitness:
            self.best_position = self.position.copy()
            self.best_fitness = self.fitness

class Swarm:
    def __init__(self, objective_function, num_particles, max_iterations, lb, ub, w=0.5, c1=0.5, c2=0.5):
        self.objective_function = objective_function
        self.num_particles = num_particles
        self.max_iterations = max_iterations
        self.lb = lb
        self.ub = ub
        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.particles = [Particle(objective_function, lb, ub) for i in range(num_particles)]
        self.global_best_position = self.particles[0].best_position.copy()
        self.global_best_fitness = self.particles[0].best_fitness
    
    def optimize(self):
        for t in range(self.max_iterations):
            for i in range(self.num_particles):
                particle = self.particles[i]
                particle.update_velocity(self.global_best_position, self.w, self.c1, self.c2)
                particle.update_position()
                particle.update_personal_best()
                if particle.best_fitness < self.global_best_fitness:
                    self.global_best_position = particle.best_position.copy()
                    self.global_best_fitness = particle.best_fitness
            if t % 10 == 0:
                print('Iteration {}: Best Fitness = {:.6f}'.format(t, self.global_best_fitness))
    
    def get_solution(self):
        return self.global_best_position, self.global_best_fitness

# Define the objective function to be minimized
def griewank(x):
    n = len(x)
    x = [xi/100 for xi in x]
    sum_of_squares = sum([xi ** 2 for xi in x])
    product_of_cosines = 1.0
    for i in range(n):
        product_of_cosines *= math.cos(x[i] / math.sqrt(i + 1))
    return 1 + (1 / 4000) * sum_of_squares - product_of_cosines

# Define the bounds of the search space
lb = np.array([-5, -5, -5])
ub = np.array([5, 5, 5])

# Create the swarm and run the PSO algorithm
swarm = Swarm(griewank, num_particles=20, max_iterations=100, lb=lb, ub=ub)
swarm.optimize()

# Print the optimized solution and its objective value
x_opt, f_opt = swarm.get_solution()
print('Optimized solution: ', x_opt)
print('Objective value: ', f_opt)
