from random import randint

class Mutation:
    def __init__(self):
        pass

    def mutation(self, mode, solution, limits):
        self.n = len(solution.params)
        self.solution = solution
        self.limits = limits

        if mode == 'uniform':
            return self.__uniform()

    def __uniform(self):
        newSolution = []
        for i in range(self.n):
            switched = randint(0, 1)
            if switched:
                newSolution.append(randint(self.limits[i][0], self.limits[i][1]))
            else:
                newSolution.append(self.solution.params[i])
        
        return newSolution