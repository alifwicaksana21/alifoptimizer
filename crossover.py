from random import randint

class Crossover:
    def __init__(self):
        parent1 = None
        parent2 = None

    def checkParents(self, parent1, parent2):
        if not len(parent1.params) == len(parent2.params):
            raise IndexError()
    
    def crossover(self, parents, mode):
        self.parent1 = parents[0]
        self.parent2 = parents[1]
        self.checkParents(self.parent1, self.parent2)
        self.n = len(self.parent1.params)

        if mode == 'singlepoint':
            return self.__singlePoint()
        elif mode == 'uniform':
            return self.__uniform()

    def __singlePoint(self):
        
        index = randint(0, self.n-1)
        offspring1 = self.newSolution(self.parent1.params)
        offspring2 = self.newSolution(self.parent2.params)

        offspring1.params[:index] = self.parent1.params[:index]
        offspring2.params[index:] = self.parent2.params[index:]
        offspring1.params[:index] = self.parent1.params[:index]
        offspring2.params[index:] = self.parent2.params[index:]        

        return offspring1, offspring2

    def __uniform(self):
        offspring1 = []
        offspring2 = []

        for i in range(self.n):
            switched = randint(0, 1)
            if switched:
                offspring1.append(self.parent1.params[i])
                offspring2.append(self.parent2.params[i])
            else:
                offspring1.append(self.parent2.params[i])
                offspring2.append(self.parent1.params[i])
        
        return self.newSolution(offspring1), self.newSolution(offspring2)
