import numpy as np
from solution import Solution

class Optimizer:
    def __init__(self):
        self.solutions = []
        self.fitnessFunction = None
    
    def initializeSolutions(self, nSolution: int, nParams: int, limits: list):
        self.nSolution = nSolution
        self.nParams = nParams

        if not isinstance(limits[0], list):
            raise TypeError()
        
        if(len(limits) == 1):
            self.limits = limits * nParams
            
        for i in range(nSolution):
            solution = []
            for i in range(nParams):
                solution.append(np.random.randint(low=self.limits[i][0], high=self.limits[i][1]))
            self.solutions.append(Solution(solution))
        
    def sortSolutions(self, mode="ASC"):
        self.__calculateFitnesses()
        if mode == "ASC":
            self.solutions = sorted(self.solutions, key=lambda s: s.fitness)
        elif mode == "DESC":
            self.solutions = sorted(self.solutions, key=lambda s: s.fitness, reverse=True)
        else:
            raise TypeError()
    
    def setFitnessFunction(self, fitnessFunction):
        self.fitnessFunction = fitnessFunction

    def __calculateFitnesses(self):
        for i in range(len(self.solutions)):
            fitness = self.fitnessFunction(self.solutions[i].params)
            self.solutions[i].setFitness(fitness)
    
    def getHighestSolution(self):
        self.sortSolutions()
        return self.solutions[-1]
    
    def getLowestSolution(self):
        self.sortSolutions()
        return self.solutions[1]
    
    def newSolution(self, params):
        return Solution(params=params)