from random import randint
from optimizer import Optimizer
from crossover import Crossover
from mutation import Mutation

class GeneticAlgorithm(Optimizer, Crossover, Mutation):
    def __init__(self, mode):
        super().__init__()
        self.mode = mode
        self.lastBest = None
        self.crossoverMode = 'uniform'
        self.mutationMode = 'uniform'

    def fit(self, iteration, verbose=0):
        self.__isInitialized()
        self.__isFitnessDefined()

        for i in range(iteration):
            self.sortSolutions()

            while len(self.solutions) > self.nSolution:
                del self.solutions[-1]

            if verbose:
                if(self.lastBest != self.getLowestSolution().params):
                    print("Iteration : ", i+1)
                    print("Best Solution : ", self.getLowestSolution())
                    self.lastBest = self.getLowestSolution().params

            if self.mode == 0:
                parent1 = self.solutions[0]
                parent2 = self.solutions[randint(1, len(self.solutions)-1)]
            else:
                parent1 = self.solutions[-1]
                parent2 = self.solutions[randint(0, len(self.solutions)-2)]
            
            offspring1, offspring2 = self.crossover(parents=[parent1, parent2], mode=self.crossoverMode)
            
            self.solutions.append(offspring1)
            self.solutions.append(offspring2)

            idx = randint(0, len(self.solutions)-1)
            newSolution = self.mutation(mode=self.mutationMode, solution=self.solutions[idx], limits=self.limits)
            self.solutions[idx].setParams(newSolution)

    def __isInitialized(self):
        if len(self.solutions) == 0:
            raise ValueError("Solutions should be initialized first.")
    
    def __isFitnessDefined(self):
        if not self.fitnessFunction:
            raise ValueError("Fitness function should be defined first.")