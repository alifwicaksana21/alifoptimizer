import math
from geneticalgorithm import GeneticAlgorithm


def griewank(x):
    n = len(x)
    x = [xi/100 for xi in x]
    sum_of_squares = sum([xi ** 2 for xi in x])
    product_of_cosines = 1.0
    for i in range(n):
        product_of_cosines *= math.cos(x[i] / math.sqrt(i + 1))
    return 1 + (1 / 4000) * sum_of_squares - product_of_cosines

def fitness(x):
    return sum(x)

ga = GeneticAlgorithm(mode=0)
ga.initializeSolutions(5, 4, [[0, 500]])

for solution in ga.solutions:
    print(solution)
print()

ga.setFitnessFunction(fitness)
ga.fit(100, 1)